﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KavosAparatas
{
    class Program
    {

        static void Main(string[] args)
        {
            Valdymas();

            Console.ReadLine();
        }
        public static void Valdymas()
        {
            Aparatas aparatas = new Aparatas();
            while (true)
            {
                Console.WriteLine("Kokio kavos puodelio noretumete? (S, M, L)");
                string pasirinkimas = Console.ReadLine();
                aparatas.Puodelis(pasirinkimas);
                aparatas.Papildymas();
            }
        }
    }
}
