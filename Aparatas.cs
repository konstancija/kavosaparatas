﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KavosAparatas
{
    class Aparatas
    {
        public string dydis;
        float kaina = 0f;
        float kavosKiekis;
        float vandensKiekis;
        public float vandensLikutis = 5f;
        public float kavosLikutis = 3f;
        public bool sumoketa = false;
        public float idetaSuma = 0f;
        public float pinigai = 0f;

        public Aparatas()
        {

        }
        public void Puodelis(string dydis)
        {
            if (dydis == "S" && vandensLikutis >= 0.75f && kavosLikutis >= 1f)
            {
                kaina = 0.75f;
                kavosKiekis = 0.5f;
                vandensKiekis = 0.25f;
                vandensLikutis -= vandensKiekis;
                kavosLikutis -= kavosKiekis;
                PiniguIdejimas();
                Console.WriteLine(dydis + " kava paruosta!");
            }
            else if (dydis == "M" && vandensLikutis >= 0.75f && kavosLikutis >= 1f)
            {
                kaina = 1f;
                kavosKiekis = 0.75f;
                vandensKiekis = 0.5f;
                vandensLikutis -= vandensKiekis;
                kavosLikutis -= kavosKiekis;
                PiniguIdejimas();
                Console.WriteLine(dydis + " kava paruosta!");
            }
            else if (dydis == "L" && vandensLikutis >= 0.75f && kavosLikutis >= 1f)
            {
                kaina = 1.5f;
                kavosKiekis = 1f;
                vandensKiekis = 0.75f;
                vandensLikutis -= vandensKiekis;
                kavosLikutis -= kavosKiekis;
                PiniguIdejimas();
                Console.WriteLine(dydis + " kava paruosta!");
            }
            else if (dydis == "Pinigai")
            {
                Console.WriteLine("Viso uzdirbta: " + pinigai);
            }
            else
            {
                Console.WriteLine("Neteisingas pasirinkimas");
            }
        }
        public void Papildymas ()
        {
            if (vandensLikutis < 0.75f || kavosLikutis < 1f)
            {
                while (vandensLikutis < 0.75f)
                {
                    Console.WriteLine("Truksta vandens, prasome papildyti (V)");
                    string papildymas = Console.ReadLine();
                    if (papildymas == "V")
                    {
                        vandensLikutis = 5f;
                    }
                    else
                    {
                        Console.WriteLine("Neteisingas pasirinkimas, prasome kartoti");
                    }
                }
                while (kavosLikutis < 1f)
                {
                    Console.WriteLine("Truksta kavos, prasome papildyti (K)");
                    string papildymas = Console.ReadLine();
                    if (papildymas == "K")
                    {
                        kavosLikutis = 3f;
                    }
                    else
                    {
                        Console.WriteLine("Neteisingas pasirinkimas, prasome kartoti");
                    }
                }
            }
        }
        public void PiniguIdejimas()
        {
            idetaSuma = 0f;
            while (idetaSuma < kaina)
            {
                Console.WriteLine("Idekite " + (kaina - idetaSuma) + " EUR");
                float ideta = float.Parse(Console.ReadLine());
                idetaSuma += ideta;
                Pinigai();
            }
            if (idetaSuma > kaina)
            {
                float graza = idetaSuma - kaina;
                Console.WriteLine("Jusu graza: " + graza);
                pinigai -= graza;
            }
        }
        public float Pinigai ()
        {
            pinigai += idetaSuma;
            return pinigai;
        }
    }
}
